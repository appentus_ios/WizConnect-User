//
//  Model_SignIn.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire


class Model_SignIn {
    static let shared = Model_SignIn()
    
    var c_code = ""
    
    var customer_social = ""
    var customer_name=""
    var customer_email=""
    var customer_profile=""
    
    var customer_mobile = ""
    var customer_device_type = ""
    var customer_device_token = ""
    var customer_password = ""
    
    var str_OTP = ""
    
    var str_message = ""
    
    func func_SignIn(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_customer_login
        
        let str_Params = ["customer_country_code":"\(c_code)",
            "customer_mobile":"\(customer_mobile)",
            "customer_device_type":"2",
            "customer_device_token":"\(k_FireBaseFCMToken)",
            "customer_password":"\(customer_password)"]
        print(str_Params)
        
        API_WizConnect.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                
                let arr_Result = dict_JSON["result"] as? [[String:Any]]
                let dict_Result = arr_Result![0]
                
                Model_Splash.shared.custumer_ID = "\(dict_Result["customer_id"]!)"
                
                let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: dict_Result)
                UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    func func_SignIn_Social(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_customer_social_login
        
        let str_Params = "customer_social=\(customer_social)&customer_name=\(customer_name)&customer_email=\(customer_email)&customer_profile=\(customer_profile)&customer_device_type=2&customer_device_token=\(k_FireBaseFCMToken)"
        
        print(str_Params)
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                
                let arr_Result = dict_JSON["result"] as? [[String:Any]]
                let dict_Result = arr_Result![0]
                
                Model_Splash.shared.custumer_ID = "\(dict_Result["customer_id"]!)"
                
                let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: dict_Result)
                UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    
}



