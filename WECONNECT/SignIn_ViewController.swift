//
//  SignIn_ViewController.swift
//  WECONNECT
//
//  Created by Raja Vikram singh on 22/11/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//



import UIKit
import ACFloatingTextfield_Swift
import CountryPicker
import SVProgressHUD

import FBSDKLoginKit
import GoogleSignIn

let statusBarColor = UIColor (red: 164.0/255.0, green: 37.0/255.0, blue: 0.0/255.0, alpha: 1.0)
let color_AppDefault = UIColor (red: 224.0/255.0, green: 59.0/255.0, blue: 11.0/255.0, alpha: 1.0)


class SignIn_ViewController: UIViewController,CountryPickerDelegate , GIDSignInUIDelegate , GIDSignInDelegate {
    //    MARK:- IBOutlets
    @IBOutlet weak var btn_continue:UIButton!
    @IBOutlet weak var btn_fb:UIButton!
    @IBOutlet weak var btn_google:UIButton!
    @IBOutlet weak var btn_terms_conditions:UIButton!
    
    @IBOutlet weak var txt_mobile_number:ACFloatingTextfield!
    @IBOutlet weak var txt_password:ACFloatingTextfield!

    @IBOutlet weak var picker: CountryPicker!
    
    @IBOutlet weak var view_CountryPicker: UIView!
    
    @IBOutlet weak var btn_CountryCode: UIButton!

    
    //    MARK:- vars
    
    //    MARK:- VC's life Cycle
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        func_set_design()
        
        view_CountryPicker.isHidden = true
        func_AddCountryPicker()
        
        guard
            let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else {
            return
        }
        statusBarView.backgroundColor = statusBarColor
        
    }
    
    
    func func_set_design() {
        
        btn_continue.layer.cornerRadius = btn_continue.frame.size.height/2
        btn_continue.clipsToBounds = true
        
        btn_fb.layer.cornerRadius = btn_continue.frame.size.height/2
        btn_fb.layer.borderColor = UIColor (red: 224.0/255.0, green: 59.0/255.0, blue: 11.0/255.0, alpha: 1.0) .cgColor
        btn_fb.layer.borderWidth = 1
        btn_fb.clipsToBounds = true
        
        btn_google.layer.cornerRadius = btn_continue.frame.size.height/2
        btn_google.layer.borderColor = UIColor (red: 224.0/255.0, green: 59.0/255.0, blue: 11.0/255.0, alpha: 1.0) .cgColor
        btn_google.layer.borderWidth = 1
        btn_google.clipsToBounds = true
        
        let underlineAttribute : [NSAttributedStringKey: Any] = [
            NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont (name: "Lato-Regular", size:  12.0) ?? UIFont .systemFont(ofSize: 12),
            NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineColor.rawValue) : UIColor.lightGray,
            NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.lightGray,
            NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineStyle.rawValue) :NSUnderlineStyle.styleSingle.rawValue]
        
        let underlineAttributedString = NSAttributedString(string: " Terms & Conditions", attributes: underlineAttribute)
        
        btn_terms_conditions.setAttributedTitle(underlineAttributedString, for: .normal)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    MARK:- IBActions
    
    @IBAction func btn_terms_conditions(_ sender: Any) {
        let terms_condtion_VC = storyboard?.instantiateViewController(withIdentifier: "FAQ_ViewController") as! FAQ_ViewController
        present(terms_condtion_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_Continue(_ sender: Any) {
        
        if !func_Validation() {
            return
        }
        
        Model_SignIn.shared.c_code = btn_CountryCode.currentTitle!
        Model_SignIn.shared.customer_mobile = txt_mobile_number.text!
        Model_SignIn.shared.customer_password = txt_password.text!
        
        func_ShowHud()
        Model_SignIn.shared.func_SignIn { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                    self.present(sign_VC!, animated: true, completion: nil)
                } else {
                    self.func_ShowHud_Error(with:Model_SignIn.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
    @IBAction func btn_SignUp(_ sender: Any) {
        let sign_VC = storyboard?.instantiateViewController(withIdentifier: "SignUp_ViewController") as! SignUp_ViewController
        present(sign_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_CancelPicker(_ sender: UIButton) {
        view_CountryPicker.isHidden = true
    }
    
    @IBAction func btn_DoneCoutrnyPicker(_ sender: UIButton) {
        view_CountryPicker.isHidden = true
    }
    
    @IBAction func btn_CountryCode(_ sender: UIButton) {
        view_CountryPicker.isHidden = false
        self.view.endEditing(true)
    }
    
    //    MARK:- Custom functions
    @IBAction func btn_google (_ sender:UIButton) {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func btn_facebook (_ sender:UIButton) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil) {
                let fbloginresult : FBSDKLoginManagerLoginResult = result!

                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                    fbLoginManager.logOut()
                }
            }
        }

    }
    
    func func_AddCountryPicker() {
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //        picker.displayOnlyCountriesWithCodes = ["DK", "SE", "NO", "DE"] //display only
        //        picker.exeptCountriesWithCodes = ["RU"] //exept country
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
    }
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        print(phoneCode)
        print(countryCode)
        
        btn_CountryCode.setTitle(phoneCode, for:.normal)
    }
    
    func func_Validation() -> Bool {
        
        if txt_mobile_number.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Mobile Number")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_password.text!.isEmpty {
            func_ShowHud_Error(with: "Enter password")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else {
            return true
        }
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                     withError error: Error!) {

        if (error == nil) {
            // Perform any operations on signed in user here.
            let socialID = user.userID
            let name = user.profile.name
            let email = user.profile.email
            var imageUrl = ""
            
            if user.profile.hasImage{
                imageUrl = "\(user.profile.imageURL(withDimension: (200))!)"
            }
            else{
                imageUrl = ""
            }
            
            Model_SignIn.shared.customer_social = socialID!
            Model_SignIn.shared.customer_name = name!
            Model_SignIn.shared.customer_email = email!
            Model_SignIn.shared.customer_profile = imageUrl
            
            self.func_ShowHud()
            Model_SignIn.shared.func_SignIn_Social(completionHandler: { (status) in
                DispatchQueue.main.async {
                    self.func_HideHud()
                    
                    GIDSignIn.sharedInstance().signOut()
                    
                    if status == "success" {
                        let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                        self.present(sign_VC!, animated: true, completion: nil)
                    } else {
                        self.func_ShowHud_Error(with: "Try again")
                        DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                            self.func_HideHud()
                        })
                    }
                }
            })

            
        }
    }

    
    
    func getFBUserData(){
        SVProgressHUD.show()
        
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil) {
                    
                    print(result!)
                    
                    let resultJson : NSDictionary = result as! NSDictionary
                    let socialID = "\(resultJson["id"]!)"
                    let email = "\(resultJson["email"]!)"
                    
                    let name = "\(resultJson["name"]!)"
                    let imageDict : NSDictionary = resultJson["picture"] as! NSDictionary
                    let dataOne : NSDictionary = imageDict["data"] as! NSDictionary
                    let imageUrl = "\(dataOne["url"]!)"
                    
                    Model_SignIn.shared.customer_social = socialID
                    Model_SignIn.shared.customer_name = name
                    Model_SignIn.shared.customer_email = email
                    Model_SignIn.shared.customer_profile = imageUrl
                    
                    self.func_ShowHud()
                    Model_SignIn.shared.func_SignIn_Social(completionHandler: { (status) in
                        DispatchQueue.main.async {
                            self.func_HideHud()
                            
                            if status == "success" {
                                let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                                self.present(sign_VC!, animated: true, completion: nil)
                            } else {
                                self.func_ShowHud_Error(with: "Try again")
                                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                                    self.func_HideHud()
                                })
                            }
                        }
                    })
                    
//                    SVProgressHUD.dismiss()
                } else {
                    SVProgressHUD.dismiss()
                }
            })
        }
    }
    
    
    //    MARK:- Finish
}









