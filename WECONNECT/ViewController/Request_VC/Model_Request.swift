//
//  Model_Request.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 03/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire



class Model_Request {
    static let shared = Model_Request()
    
    var arr_upcoming_accepted_jobs = [Model_Request]()
    var arr_completed_jobs = [Model_Request]()
    
    var str_message = ""
    
    var booking_id =  ""
    var booking_date =  ""
    var booking_customer_id =  ""
    var booking_vendor_id =  ""
    var booking_when =  ""
    var booking_where =  ""
    var booking_lat =  ""
    var booking_long =  ""
    var booking_address =  ""
    var booking_full_address =  ""
    var booking_landmark =  ""
    var booking_cat =  ""
    var booking_subcat =  ""
    var booking_charge =  ""
    var booking_status =  ""
    var payment_status =  ""
    var vendor_id =  ""
    var vendor_name =  ""
    var vendor_email =  ""
    var vendor_social =  ""
    var vendor_country_code =  ""
    var vendor_mobile =  ""
    var vendor_otp =  ""
    var vendor_profile =  ""
    var vendor_about =  ""
    var vendor_cost =  ""
    var vendor_device_type =  ""
    var vendor_device_token =  ""
    var vendor_location =  ""
    var vendor_lat =  ""
    var vendor_long =  ""
    var vendor_password =  ""
    var vendor_doc1 =  ""
    var vendor_doc2 =  ""
    var vendor_doc3 =  ""
    var vendor_hour_rate =  ""
    var vendor_cate_code =  ""
    var vendor_subcate_code =  ""
    var plan_code =  ""
    var vendor_status =  ""
    var category_name =  ""
    var sub_cate_name =  ""

    
    
    func func_get_customer_bookings(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_customer_bookings
        let str_params = "customer_id=\(Model_Splash.shared.custumer_ID)"

        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_upcoming_accepted_jobs.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                let arr_Results = dict_JSON["result"] as! [[String:Any]]
                
                for dict in arr_Results {
                    self.arr_upcoming_accepted_jobs.append(self.func_set_data(dict: dict))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    
    
    func func_get_customer_complete_bookings(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_customer_complete_bookings
        let str_params = "customer_id=\(Model_Splash.shared.custumer_ID)"
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_completed_jobs.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                let arr_Results = dict_JSON["result"] as! [[String:Any]]
                
                for dict in arr_Results {
                    self.arr_completed_jobs.append(self.func_set_data(dict: dict))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_set_data(dict:[String:Any]) -> Model_Request {
        let model = Model_Request()
        
        model.booking_id  = "\(dict["booking_id"] ?? "")"
        model.booking_date  = "\(dict["booking_date"] ?? "")"
        model.booking_customer_id  = "\(dict["booking_customer_id"] ?? "")"
        model.booking_vendor_id  = "\(dict["booking_vendor_id"] ?? "")"
        model.booking_when  = "\(dict["booking_when"] ?? "")"
        model.booking_where  = "\(dict["booking_where"] ?? "")"
        model.booking_lat  = "\(dict["booking_lat"] ?? "")"
        model.booking_long  = "\(dict["booking_long"] ?? "")"
        model.booking_address  = "\(dict["booking_address"] ?? "")"
        model.booking_full_address  = "\(dict["booking_full_address"] ?? "")"
        model.booking_landmark  = "\(dict["booking_landmark"] ?? "")"
        model.booking_cat  = "\(dict["booking_cat"] ?? "")"
        model.booking_subcat  = "\(dict["booking_subcat"] ?? "")"
        model.booking_charge  = "\(dict["booking_charge"] ?? "")"
        model.booking_status  = "\(dict["booking_status"] ?? "")"
        model.payment_status  = "\(dict["payment_status"] ?? "")"
        
        model.vendor_id  = "\(dict["vendor_id"] ?? "")"
        model.vendor_name  = "\(dict["vendor_name"] ?? "")"
        model.vendor_email  = "\(dict["vendor_email"] ?? "")"
        model.vendor_social  = "\(dict["vendor_social"] ?? "")"
        model.vendor_country_code  = "\(dict["vendor_country_code"] ?? "")"
        model.vendor_mobile  = "\(dict["vendor_mobile"] ?? "")"
        model.vendor_otp  = "\(dict["vendor_otp"] ?? "")"
        model.vendor_profile  = "\(dict["vendor_profile"] ?? "")"
        model.vendor_about  = "\(dict["vendor_about"] ?? "")"
        model.vendor_cost  = "\(dict["vendor_cost"] ?? "")"
        model.vendor_device_type  = "\(dict["vendor_device_type"] ?? "")"
        model.vendor_device_token  = "\(dict["vendor_device_token"] ?? "")"
        model.vendor_location  = "\(dict["vendor_location"] ?? "")"
        model.vendor_lat  = "\(dict["vendor_lat"] ?? "")"
        model.vendor_long  = "\(dict["vendor_long"] ?? "")"
        model.vendor_password  = "\(dict["vendor_password"] ?? "")"
        model.vendor_doc1  = "\(dict["vendor_doc1"] ?? "")"
        model.vendor_doc2  = "\(dict["vendor_doc2"] ?? "")"
        model.vendor_doc3  = "\(dict["vendor_doc3"] ?? "")"
        model.vendor_hour_rate  = "\(dict["vendor_hour_rate"] ?? "")"
        model.vendor_cate_code  = "\(dict["vendor_cate_code"] ?? "")"
        model.vendor_subcate_code  = "\(dict["vendor_subcate_code"] ?? "")"
        model.plan_code  = "\(dict["plan_code"] ?? "")"
        model.vendor_status  = "\(dict["vendor_status"] ?? "")"
        model.category_name  = "\(dict["category_name"] ?? "")"
        model.sub_cate_name  = "\(dict["sub_cate_name"] ?? "")"
        
        return model
    }
    
}
