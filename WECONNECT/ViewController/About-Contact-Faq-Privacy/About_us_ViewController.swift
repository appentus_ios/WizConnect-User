//
//  About_us_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 08/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//

import UIKit

class About_us_ViewController: UIViewController {

    @IBOutlet weak var web_view: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        web_view.loadRequest(NSURLRequest(url: NSURL(fileURLWithPath: Bundle.main.path(forResource: "about", ofType: "html")!) as URL) as URLRequest)
        
        
        // Do any additional setup after loading the view.
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
