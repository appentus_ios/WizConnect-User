//
//  WalkThrough_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 15/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class WalkThrough_ViewController: UIViewController {
    
    @IBOutlet weak var coll_car:UICollectionView!
    
    @IBOutlet weak var btn_Next:UIButton!
    @IBOutlet weak var btn_Skip:UIButton!
    
    @IBOutlet weak var view_next_skip:UIView!
    
    var index = 0
        
    var arr_Walkthrought_images  = ["Full Album pictures","Aluminum work","Artist.png","Barbing.png","Cab Operation.png","Capenter.png","Computer.png","Dj.png","Electricians.png","Field Technicians.png","Hair Salon.png","Loan.png","Mechanic.png","paiting.png","Photography.png","Private Coaching.png","Tailoring.png","Towing Van.png","Welding.png"]
    
    var arr_Walkthrought_Title = ["Welcome to WizConnect","Aluminum work","Artist are available","Barbing facilities","Cabs are availble","Carpentry work","Computer work","Available DJ for party","Electrical work","Field Technicians","Salon Facilities available","Loan facilities","Mechanic work","Painting work","Photography (Photo shoot)","Private coaching are availble","Tailoring work","Towing services","Welding work"]

    var arr_Walkthrought_Description = ["WizConnect gives you access to hub of all kind of service providers at the compfort of your mobile device.",
                                        "From the large extrusion industries to dealers of aluminium and allied products, including glaciers and technicians, you will find them all here.",
                                        "You can connect here with Fine artists, Illustration artists, Sculptural, Drawing artists and more.",
                                        "You may choose from a list of Barbers and Barbing facilities nearest to your current location.",
                                        "Lookout for Cabs or keke around you and either contact them now or schedule an appointment",
                                        "You will find both construction and domestic carpenters here who provide services that suit your immediate need.","Programmers, Web designers and developers, graphic artists, system engineers and many more all in one place.",
                                        "Hire DJ or MC at the comfort of your mobile device. You may view ratings to guide you or choose the ones closest to you.",
                                        "From Electrical Engineering professionals to local technicians, we have them all waiting to servce you.",
                                        "Hire field technicians to support your project or construction needs.",
                                        "Discover hair stylists and schedule appointments.",
                                        "Be it personal loan, agricultural loan, salary upfront or mortage facilities, you can connect here with institutions that best serve your needs.",
                                        "Find auto mechanics, gererator mechanics, keke and motorcycle mechanics, boat and ship mechanics.",
                                        "You can find painters here for your home and automobile needs.",
                                        "Book photography sessions, events coverage or training sessions. Best deals available here.",
                                        "Find the right personal coach for your children's academic needs. You will also discover life coaches, marriage and career counsellors.",
                                        "Discover the best fashion houses around your area and also find solutions to minor tailoring needs ike stitching, patching and mending of wears.",
                                        "Discover towing services from generators to small cars and heavy duty construction vehicles.",
                                        "Discover sub-sea welders, construction welders, domestic and mobile welders."]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_next_skip.layer.cornerRadius = view_next_skip.frame.size.height/2
        view_next_skip.clipsToBounds = true
        
        btn_Next.layer.cornerRadius = btn_Next.frame.size.height/2
        btn_Next.layer.borderColor = UIColor (red: 224.0/255.0, green: 59.0/255.0, blue: 11.0/255.0, alpha: 1.0) .cgColor
        btn_Next.layer.borderWidth = 1
        btn_Next.layer.shadowOpacity = 1.0
        btn_Next.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        btn_Next.layer.shadowRadius = 3.0
        btn_Next.layer.shadowColor = UIColor .lightGray.cgColor
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_next(_ sender: Any) {
        
        let collectionBounds = coll_car.bounds
        let contentOffset = CGFloat(CGFloat(index) * coll_car.frame.size.width + collectionBounds.size.width)
//        CGFloat(floor(coll_car.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
        
        if contentOffset == CGFloat(arr_Walkthrought_Title.count)*CGFloat(collectionBounds.size.width) {
            let sign_VC = storyboard?.instantiateViewController(withIdentifier: "SignIn_ViewController") as! SignIn_ViewController
            present(sign_VC, animated: true, completion: nil)
        }
        
        index = index+1
    }
    
    @IBAction func btn_Skip(_ sender: Any) {
        let sign_VC = storyboard?.instantiateViewController(withIdentifier: "SignIn_ViewController") as! SignIn_ViewController
        present(sign_VC, animated: true, completion: nil)
    }
    
    
}



//  MARK:- UICollectionView methods
extension WalkThrough_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_Walkthrought_Title.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let coll_Car = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! WalkThroughCollectionViewCell
        
        coll_Car.img_work.image = UIImage (named: arr_Walkthrought_images[indexPath.row])
        coll_Car.lbl_Title.text=arr_Walkthrought_Title[indexPath.row]
        coll_Car.lbl_description.text = arr_Walkthrought_Description[indexPath.row]
        
        return coll_Car
    }

    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : coll_car.contentOffset.y ,width : coll_car.frame.width,height : coll_car.frame.height)
        coll_car.scrollRectToVisible(frame, animated: true)
    }

}





