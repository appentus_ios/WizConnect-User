//
//  Model_Vender_list.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 21/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation

class Model_Vender_list {
    
    static let shared = Model_Vender_list()
    
    var arr_sub_category_list = [Model_Vender_list]()
    
    var lat = ""
    var long =  ""
    var range =  "5"
    
    var vendor_id = ""
    var vendor_name = ""
    var vendor_email = ""
    var vendor_social = ""
    var vendor_mobile = ""
    var vendor_otp = ""
    var vendor_profile = ""
    var vendor_cost = ""
    var vendor_device_type = ""
    var vendor_device_token = ""
    var vendor_location = ""
    var vendor_lat = ""
    var vendor_long = ""
    var vendor_password = ""
    var vendor_about = ""
    var vendor_hour_rate = ""
    var vendor_cate_code = ""
    var vendor_subcate_code = ""
    var plan_code = ""
    var vendor_status = ""
    var category_id = ""
    var category_name = ""
    var category_icon = ""
    var category_code = ""
    var feedback_id = ""
    var sender_id = ""
    var receiver_id = ""
    var comment = ""
    var rating = ""
    var date = ""
    var rating_avg = ""
    var distance = ""
    
//    var model_Vender_selected = Model_Vender_list()
    
    func func_vendor_list(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_vendor_list
        let str_param = "category=\(Model_Categories.shared.category_code)&sub_category=\(Model_Sub_Category.shared.sub_cate_code)&lat=\(lat)&lang=\(long)&range=\(range)"
        print(str_param)
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_param) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String ==  "success" {
                
                let arr_Result = dict_JSON["result"] as? [[String:Any]]
                
                for dict_result in arr_Result! {
                    self.arr_sub_category_list.append(self.func_set_category_Data(dict: dict_result))
                }
            }
            completionHandler(dict_JSON["status"] as! String)
        }
    }
    
    
    
     func func_set_category_Data(dict:[String:Any]) -> Model_Vender_list {
        let model = Model_Vender_list()
        
        model.vendor_id = dict["vendor_id"] as! String
        model.vendor_name = dict["vendor_name"] as! String
        model.vendor_email = dict["vendor_email"] as! String
        model.vendor_social = dict["vendor_social"] as! String
        model.vendor_mobile = dict["vendor_mobile"] as! String
        model.vendor_otp = dict["vendor_otp"] as! String
        model.vendor_profile = dict["vendor_profile"] as! String
        model.vendor_cost = dict["vendor_cost"] as! String
        model.vendor_device_type = dict["vendor_device_type"] as! String
        model.vendor_device_token = dict["vendor_device_token"] as! String
        model.vendor_location = dict["vendor_location"] as! String
        model.vendor_lat = dict["vendor_lat"] as! String
        model.vendor_long = dict["vendor_long"] as! String
        model.vendor_password = dict["vendor_password"] as! String
        model.vendor_about = dict["vendor_about"] as! String
        
        model.vendor_hour_rate = dict["vendor_hour_rate"] as! String
        model.vendor_cate_code = dict["vendor_cate_code"] as! String
        model.vendor_subcate_code = dict["vendor_subcate_code"] as! String
        model.plan_code = dict["plan_code"] as! String
        model.vendor_status = dict["vendor_status"] as! String
        model.category_id = dict["category_id"] as! String
        model.category_name = dict["category_name"] as! String
        model.category_icon = dict["category_icon"] as! String
        model.category_code = dict["category_code"] as! String
        model.feedback_id = "\(dict["feedback_id"]!)"
        model.sender_id = "\(dict["sender_id"]!)"
        model.receiver_id = "\(dict["receiver_id"]!)"
        model.comment = dict["comment"] as! String
        let str_rating = "\(dict["rating"]!)"
        if str_rating.isEmpty {
            model.rating = "0"
        } else {
            model.rating = str_rating
        }
        
        model.date = dict["date"] as! String
        
        let str_rating_avg = "\(dict["rating_avg"]!)"
        if str_rating_avg.isEmpty {
            model.rating_avg = "0"
        } else {
            model.rating_avg = str_rating_avg
        }
        
        let distance = dict["distance"] as! String
        model.distance = String(format: "%.2f",Double(distance)!)
        
        return model
    }
    
    
}


