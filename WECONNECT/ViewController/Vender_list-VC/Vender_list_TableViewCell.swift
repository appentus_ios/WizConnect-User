//
//  Category_wise_list_TableViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 19/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Vender_list_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var view_container:UIView!
    @IBOutlet weak var img_user:UIImageView!
    
    @IBOutlet weak var lbl_vender_name:UILabel!
    @IBOutlet weak var lbl_category_name:UILabel!
    @IBOutlet weak var lbl_distance:UILabel!
    @IBOutlet weak var lbl_rating:UILabel!
    @IBOutlet weak var lbl_rating_avg:UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        img_user.layer.cornerRadius = img_user.frame.size.height/2
        img_user.clipsToBounds = true
        
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_container.layer.shadowRadius = 1.5
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    
}

