//
//  Category_wise_list_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 19/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage


class Vender_list_ViewController: UIViewController {
    @IBOutlet weak var view_container:UIView!
    @IBOutlet weak var btn_switchToMapView:UIButton!
    
    @IBOutlet weak var tbl_Category:UITableView!
    @IBOutlet weak var mapview:GMSMapView!
    
    @IBOutlet weak var lbl_sub_category_name:UILabel!
    
    var locationManager = CLLocationManager()
    
    var co_OrdinateCurrent = CLLocationCoordinate2DMake(0.0, 0.0)
    
    var timer : Timer!
    
    @IBOutlet weak var txt_search:UITextField!
    @IBOutlet weak var btn_search:UIButton!
    
    var arr_searched = [Model_Vender_list]()
    var arr_for_search = [Model_Vender_list]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        btn_switchToMapView.layer.borderColor = color_AppDefault .cgColor
        btn_switchToMapView.layer.borderWidth = 1
        
        view_container.layer.cornerRadius = 4
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_container.layer.shadowRadius = 3
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
        
        tbl_Category.isHidden = false
        mapview.isHidden = true
        
        lbl_sub_category_name.text = Model_Sub_Category.shared.sub_cate_name
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        txt_search.isHidden = true
        txt_search.text = ""
        btn_search.isSelected = false
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(func_Timer), userInfo: nil, repeats: true)
        func_ShowHud()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func btn_switchToMapView(_ sender:UIButton) {
        
        if sender.currentTitle == "Switch to List" {
            sender.setTitle("Switch to Map View", for: .normal)
        } else {
            sender.setTitle("Switch to List", for: .normal)
        }
        
        if tbl_Category.isHidden {
            tbl_Category.isHidden = false
        } else {
            tbl_Category.isHidden = true
        }
        
        if mapview.isHidden {
            mapview.isHidden = false
            
            func_show_venders_mapview()
        } else {
            mapview.isHidden = true
        }
        
    }
    
    func func_show_venders_mapview() {
        mapview.clear()
        
        var marker = GMSMarker()
        var bounds = GMSCoordinateBounds()
        
        for model in Model_Vender_list.shared.arr_sub_category_list {
            var cord = CLLocationCoordinate2DMake(0, 0)
            
            if !model.vendor_lat.isEmpty {
                cord = CLLocationCoordinate2DMake(Double(model.vendor_lat)!, Double(model.vendor_long)!)
            }
            
            marker = GMSMarker(position: cord)
            
            let custom_marker = UIView (frame: CGRect (x: 0, y: 0, width: 60, height: 60))
            custom_marker.backgroundColor = UIColor .red
            
            custom_marker.layer.cornerRadius = custom_marker.frame.size.height/2
            custom_marker.clipsToBounds = true
            
            let img_custom_marker = UIImageView (frame: custom_marker.frame)
            
            img_custom_marker.sd_setShowActivityIndicatorView(true)
            img_custom_marker.sd_setIndicatorStyle(.gray)
            img_custom_marker.sd_setImage(with:URL (string: model.vendor_profile), placeholderImage:(UIImage(named:"user-gray.png")))
            
            img_custom_marker.backgroundColor = UIColor .white
            custom_marker.addSubview(img_custom_marker)
            marker.iconView = custom_marker
            marker.map = self.mapview
            bounds = bounds .includingCoordinate(cord)
        }
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
        mapview.animate(with: update)
    }
    
   
    @objc func func_Timer()  {
        if co_OrdinateCurrent.latitude != 0.0 {
            timer.invalidate()
           
            Model_Vender_list.shared.lat = "\(co_OrdinateCurrent.latitude)"
            Model_Vender_list.shared.long = "\(co_OrdinateCurrent.longitude)"
            
            Model_Vender_list.shared.arr_sub_category_list.removeAll()
            
            func_ShowHud()
            Model_Vender_list.shared.func_vendor_list { (status) in
                DispatchQueue.main.async {
                    self.func_HideHud()
                    self.arr_for_search = Model_Vender_list.shared.arr_sub_category_list
                    self.tbl_Category.reloadData()
                }
            }
        }
    }
    
    
    @IBAction func btn_Search(_ sender: UIButton) {
        btn_search.isSelected = !(btn_search.isSelected)
        
        if txt_search.isHidden {
            txt_search.isHidden = false
        } else {
            txt_search.isHidden = true
            txt_search.text = ""
            Model_Vender_list.shared.arr_sub_category_list = arr_for_search
        }
        
    }
    
    @IBAction func txt_Search(_ sender: UITextField) {
        arr_searched = [Model_Vender_list]()
        
        for i in 0..<arr_for_search.count {
            let model = arr_for_search[i]
            let target = model.vendor_name
            if ((target as NSString?)?.range(of:txt_search.text!, options: .caseInsensitive))?.location == NSNotFound
            { } else {
                arr_searched.append(model)
            }
        }
        
        if (txt_search.text! == "") {
            arr_searched = arr_for_search
        }
        
        Model_Vender_list.shared.arr_sub_category_list = arr_searched
        if Model_Sub_Category.shared.arr_sub_category_list.count == 0 {
            tbl_Category.isHidden = true
        } else {
            tbl_Category.isHidden = false
        }
        
        tbl_Category.reloadData()
    }
    
    
}


extension Vender_list_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Vender_list.shared.arr_sub_category_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Vender_list_TableViewCell
        
        let model = Model_Vender_list.shared.arr_sub_category_list[indexPath.row]
        
        cell.img_user.sd_setShowActivityIndicatorView(true)
        cell.img_user.sd_setIndicatorStyle(.gray)
        cell.img_user.sd_setImage(with:URL (string: model.vendor_profile), placeholderImage:(UIImage(named:"user-gray.png")))
        
        cell.lbl_vender_name.text = model.vendor_name
        cell.lbl_category_name.text = " | " + model.category_name
        cell.lbl_rating.text = "(" + model.rating + ")"
        cell.lbl_rating_avg.text = model.rating_avg
        cell.lbl_distance.text = "\(model.distance) km away"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let model = Model_Vender_list.shared.arr_sub_category_list[indexPath.row]
        
        Model_Vender_list.shared.vendor_id = model.vendor_id
        Model_Vender_list.shared.vendor_profile = model.vendor_profile
        Model_Vender_list.shared.vendor_name = model.vendor_name
        Model_Vender_list.shared.category_name = model.category_name
        Model_Vender_list.shared.distance = model.distance
        Model_Vender_list.shared.vendor_cost = model.vendor_cost
        Model_Vender_list.shared.rating = model.rating
        Model_Vender_list.shared.rating_avg = model.rating_avg
        Model_Vender_list.shared.comment = model.comment
        Model_Vender_list.shared.vendor_about = model.vendor_about
        Model_Vender_list.shared.vendor_cate_code = model.vendor_cate_code
        Model_Vender_list.shared.vendor_subcate_code = model.vendor_subcate_code
        Model_Vender_list.shared.vendor_mobile = model.vendor_mobile
        
        
        Model_Chat.shared.fri_id = model.vendor_id
        Model_Chat.shared.fri_name = model.vendor_name
        Model_Chat.shared.fri_img = model.vendor_profile
        
        let plan_packages = storyboard?.instantiateViewController(withIdentifier: "Book_ViewController") as! Book_ViewController
        present(plan_packages, animated: true, completion: nil)
    }
    
}



extension Vender_list_ViewController:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        co_OrdinateCurrent = manager.location!.coordinate
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
}





