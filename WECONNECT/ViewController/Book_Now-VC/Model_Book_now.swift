//
//  Model_Book_now.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 21/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation


class Model_Book_now {
    static let shared = Model_Book_now()
    
    var booking_customer_id = ""
    var booking_vendor_id = ""
    var booking_when = ""
    var booking_where = ""
    var booking_date = ""
    
    var booking_lat = ""
    var booking_long = ""
    var booking_full_address = ""
    var booking_landmark = ""
    var booking_cat = ""
    var booking_subcat = ""
    var booking_charge = ""
    var payment_status = "0"
    var booking_address = ""
    
    var str_message = ""
    
    
    func func_book_vendor(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_book_vendor
        
        let str_Params = ["booking_customer_id":"\(booking_customer_id)",
            "booking_vendor_id":"\(booking_vendor_id)",
            "booking_when":"\(booking_when)",
            "booking_where":"\(booking_where)",
            "booking_address":"\(booking_address)",
            "booking_date":"\(booking_date)",
            "booking_lat":"\(booking_lat)",
            "booking_long":"\(booking_long)",
            "booking_full_address":"\(booking_full_address)",
            "booking_landmark":"\(booking_landmark)",
            "booking_cat":"\(booking_cat)",
            "booking_subcat":"\(booking_subcat)",
            "booking_charge":"\(booking_charge)",
            "payment_status":"\(payment_status)"]
        
        print(str_Params)
        
        API_WizConnect.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if let str_MSG = dict_JSON["message"] as? String {
                self.str_message = str_MSG
            }
            completionHandler(dict_JSON["status"] as! String)
        }
        

    }
    
}



