//
//  Book_Now_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 21/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import FSCalendar
import GoogleMaps
import GooglePlaces


class Book_Now_ViewController: UIViewController {
    @IBOutlet weak var txt_When:UITextField!
    @IBOutlet weak var txt_where:UITextField!
    
    @IBOutlet weak var txt_house_num:UITextField!
    @IBOutlet weak var txt_building_society_name:UITextField!
    @IBOutlet weak var txt_full_address:UITextField!
    @IBOutlet weak var txt_landmark:UITextField!
//    @IBOutlet weak var txt_amout:UITextField!

    @IBOutlet weak var view_datePicker:UIView!

    @IBOutlet weak var img_vender_Profile:UIImageView!
    @IBOutlet weak var lbl_vender_name:UILabel!
    @IBOutlet weak var lbl_cate_name:UILabel!
    @IBOutlet weak var lbl_sub_cate_name:UILabel!

    @IBOutlet weak var calender: FSCalendar!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if is_from_jobDetails {
            calender.select(convertNextDate(dateString:model_selected_booking.booking_date))
            
            Model_Book_now.shared.booking_date = model_selected_booking.booking_date
            txt_When.text = model_selected_booking.booking_when
            txt_where.text=model_selected_booking.booking_where
            
            let arr_house_num_buil_sociatyname = model_selected_booking.booking_address.components(separatedBy: " ")
            if arr_house_num_buil_sociatyname.count > 0 {
                txt_house_num.text=arr_house_num_buil_sociatyname[0]
                
                var str_building_society_name = ""
                for str_building_society_name_1 in arr_house_num_buil_sociatyname {
                    str_building_society_name = str_building_society_name+str_building_society_name_1
                }
                txt_building_society_name.text=str_building_society_name
            }
            
            txt_full_address.text=model_selected_booking.booking_full_address
            txt_landmark.text=model_selected_booking.booking_landmark
        }
        
        calender.scope = .week
        
        view_datePicker.isHidden = true
        
        img_vender_Profile.layer.cornerRadius = img_vender_Profile.frame.size.height/2
        img_vender_Profile.clipsToBounds=true
        
        img_vender_Profile.sd_setShowActivityIndicatorView(true)
        img_vender_Profile.sd_setIndicatorStyle(.gray)
        img_vender_Profile.sd_setImage(with:URL (string:Model_Vender_list.shared.vendor_profile), placeholderImage:(UIImage(named:"user-gray.png")))
        
        lbl_vender_name.text = Model_Vender_list.shared.vendor_name
        lbl_cate_name.text = "| "+Model_Vender_list.shared.category_name
        lbl_sub_cate_name.text = Model_Sub_Category.shared.sub_cate_name
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        
        Model_Book_now.shared.booking_date = formatter .string(from: Date())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(func_Send_Location), name:Notification.Name(rawValue: "send_Location_Chat"), object: nil)
    }

    @objc func func_Send_Location() {
        txt_where.text = str_selectYourLocations
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
            
    }
    
    @IBAction func btn_tick_Already_money_via_bank(_ sender:UIButton) {
        sender.isSelected = !(sender.isSelected)
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_when(_ sender:UIButton) {
         view_datePicker.isHidden = false
    }
    
    @IBAction func btn_Done_DatePicker(_ sender:UIButton) {
         view_datePicker.isHidden = true
    }
    
    @IBAction func date_Picker(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        txt_When.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func btn_where(_ sender:UIButton) {
        let map_VC = storyboard?.instantiateViewController(withIdentifier: "Map_ViewController") as! Map_ViewController
        present(map_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_tick_for_book(_ sender:Any) {

        if !func_Validation() {
            return
        }
        
        func_ShowHud()
        
        Model_Book_now.shared.booking_vendor_id = Model_Vender_list.shared.vendor_id
        Model_Book_now.shared.booking_customer_id = Model_Splash.shared.custumer_ID
        Model_Book_now.shared.booking_when = txt_When.text!
        Model_Book_now.shared.booking_where = txt_where.text!
        Model_Book_now.shared.booking_address = "\(txt_house_num.text!), \(txt_building_society_name.text!)"
        Model_Book_now.shared.booking_lat = "\(co_Ordinate_selected.latitude)"
        Model_Book_now.shared.booking_long = "\(co_Ordinate_selected.longitude)"
        
        Model_Book_now.shared.booking_landmark = txt_landmark.text!
        Model_Book_now.shared.booking_full_address = txt_full_address.text!
        
        Model_Book_now.shared.booking_cat = Model_Categories.shared.category_code
        Model_Book_now.shared.booking_subcat = Model_Sub_Category.shared.sub_cate_code
//        Model_Book_now.shared.booking_charge = txt_amout.text!
        
        Model_Book_now.shared.func_book_vendor { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Book_now.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        
                        self.dismiss(animated: true, completion: nil)
                    })
                } else {
                    self.func_ShowHud_Error(with: "Try again")
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
    func func_Validation() -> Bool {
        
        if txt_When.text!.isEmpty {
            func_ShowHud_Error(with: "Enter select preffered timings")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        } else if txt_where.text!.isEmpty {
            func_ShowHud_Error(with: "Enter select address")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        } else if txt_house_num.text!.isEmpty {
            func_ShowHud_Error(with: "Enter house number")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        } else if txt_building_society_name.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Building / Society name")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        }  else if txt_full_address.text!.isEmpty {
            func_ShowHud_Error(with: "Enter your address")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        } else if txt_landmark.text!.isEmpty{
            func_ShowHud_Error(with: "Enter your landmark")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        } else {
            return true
        }
    }
    
}



extension Book_Now_ViewController : FSCalendarDelegate,FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        
        Model_Book_now.shared.booking_date = formatter .string(from: date)
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Calendar.current.date(byAdding: .day, value: 0, to: Date())!
    }
    
//    func maximumDate(for calendar: FSCalendar) -> Date {
//        return Calendar.current.date(byAdding: .day, value: 15, to: Date())!
//    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        calender.scope = .week
    }
    
    
    
    func convertNextDate(dateString : String) -> Date {
        print(dateString)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let myDate = dateFormatter.date(from: dateString)!
        print(myDate)
        
        return myDate
    }
    
}


