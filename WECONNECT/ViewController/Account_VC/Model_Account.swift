//
//  Model_SignIn.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire


class Model_Account {
    static let shared = Model_Account()
    
    var customer_img = UIImage()
    var customer_email = ""
    var customer_country_code = ""
    var customer_mobile = ""
    var customer_name = ""
    var customer_device_type = ""
    var customer_device_token = ""
    
    var str_message = ""
    
    func func_update_profile_customer(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_update_profile_customer
        
        let params = ["customer_id":Model_Splash.shared.custumer_ID,
            "customer_country_code":"\(customer_country_code)",
            "customer_mobile":"\(customer_mobile)",
            "customer_name":"\(customer_name)",
            "customer_email":"\(customer_email)",
            "customer_device_type":"2",
            "customer_device_token":"\(k_FireBaseFCMToken)"]
        print(params)
        
        API_WizConnect.func_UploadWithImage(endUrl: str_FullURL, imageData: UIImageJPEGRepresentation(customer_img, 0.2), parameters: params) {
            (dict_JSON) in
            
            if dict_JSON["status"] as? String == "success" {
                let arr_Result = dict_JSON["result"] as? [[String:Any]]
                let dict_Result = arr_Result![0]
                
                self.str_message = dict_JSON["message"] as! String
                
                if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
                    var dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
                    print(dict_LoginData)
                    
                    dict_LoginData["customer_country_code"] = dict_Result["customer_country_code"]!
                    dict_LoginData["customer_device_token"] = dict_Result["customer_device_token"]!
                    dict_LoginData["customer_email"] = dict_Result["customer_email"]!
                    dict_LoginData["customer_mobile"] = dict_Result["customer_mobile"]!
                    dict_LoginData["customer_name"] = dict_Result["customer_name"]!
                    dict_LoginData["customer_profile"] = dict_Result["customer_profile"]!
                    
                    let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: dict_LoginData)
                    UserDefaults.standard .setValue(data_dict_Result, forKey: "login_Data")
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }

}



