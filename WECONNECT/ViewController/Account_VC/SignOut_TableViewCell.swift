//
//  SignOut_TableViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class SignOut_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var view_container:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        view_container.layer.cornerRadius = 2
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        view_container.layer.shadowRadius = 1.0
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    
}
