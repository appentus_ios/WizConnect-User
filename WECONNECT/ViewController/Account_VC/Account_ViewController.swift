//
//  Account_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Account_ViewController: UIViewController {
    @IBOutlet weak var tbl_account:UITableView!
    
    var arr_menu_title = ["","About Us","FAQs and Terms ","Contact Us","Privacy Policy","",]
    var arr_menu_images = ["","about-us.png","terms&conditions.png","contact-us.png","privacy-policy.png","",]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(func_reload_account), name: NSNotification.Name (rawValue: "update_Ac"), object: nil)
    }

    @objc func func_reload_account() {
       tbl_account.reloadData()
    }
    
}





extension Account_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row  == 0 {
            return 80
        } else {
            return 55
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row  == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath) as! Account_TableViewCell
            
            if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
                let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
                print(dict_LoginData)
                
                let str_img = dict_LoginData["customer_profile"] as! String
                
                cell.img_user.sd_setShowActivityIndicatorView(true)
                cell.img_user.sd_setIndicatorStyle(.gray)
                cell.img_user.sd_setImage(with:URL (string: str_img), placeholderImage:(UIImage(named:"user-gray.png")))
                
                cell.lbl_name.text = dict_LoginData["customer_name"] as? String
                cell.lbl_number.text = dict_LoginData["customer_mobile"] as? String
            }
            
            return cell
        }  else if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath)  as! Account_Menu_TableViewCell
            
            cell.lbl_menu_name.text = arr_menu_title[indexPath.row]
            cell.img_user.image = UIImage (named: arr_menu_images[indexPath.row])
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0  {
            let vc = storyboard?.instantiateViewController(withIdentifier: "MyProfile_ViewController") as! MyProfile_ViewController
            present(vc, animated: true, completion: nil)
        } else if indexPath.row == 1 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "About_us_ViewController") as! About_us_ViewController
            present(vc, animated: true, completion: nil)
        } else if indexPath.row == 2 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "FAQ_ViewController") as! FAQ_ViewController
            present(vc, animated: true, completion: nil)
        } else if indexPath.row == 3 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "Contact_us_ViewController") as! Contact_us_ViewController
            present(vc, animated: true, completion: nil)
        } else if indexPath.row == 4 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "Privacy_Policy_ViewController") as! Privacy_Policy_ViewController
            present(vc, animated: true, completion: nil)
        }  else if indexPath.row == 5 {
            let alert = UIAlertController (title: "", message: "Do you want to logout ?", preferredStyle: .alert)
            let yes = UIAlertAction(title: "Yes", style: .default) { (yes) in
                UserDefaults.standard.removeObject(forKey: "login_Data")
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignIn_ViewController") as! SignIn_ViewController
                self.present(vc, animated: true, completion: nil)
            }
            let no = UIAlertAction(title: "No", style: .default) { (yes) in
                
            }
            
            alert.addAction(yes)
            alert.addAction(no)
            
            alert.view.tintColor = UIColor .black
            present(alert, animated: true, completion: nil)
        }
    }
    
}


