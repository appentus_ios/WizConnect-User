//
//  ViewController.swift
//  WECONNECT
//
//  Created by Raja Vikram singh on 22/11/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Splash_ViewController: UIViewController {
    
    //    MARK:- IBOutlets
    
    //    MARK:- vars
    
    //    MARK:- VC's life Cycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            
            if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
                let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
                
                Model_SignIn.shared.c_code = dict_LoginData["customer_country_code"]! as! String
                Model_SignIn.shared.customer_mobile = dict_LoginData["customer_mobile"]! as! String
                Model_SignIn.shared.customer_password = dict_LoginData["customer_password"] as! String
                if k_FireBaseFCMToken.isEmpty {
                    k_FireBaseFCMToken = dict_LoginData["customer_device_token"] as! String
                }
                
                self.func_login()
            } else {
                let signIn_VC = self.storyboard?.instantiateViewController(withIdentifier: "WalkThrough_ViewController") as! WalkThrough_ViewController
                self.present(signIn_VC, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    MARK:- IBActions
    
    //    MARK:- Custom functions
     func func_login() {
        
        func_ShowHud()
        Model_SignIn.shared.func_SignIn { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                    self.present(sign_VC!, animated: true, completion: nil)
                }  else {
                    let signIn_VC = self.storyboard?.instantiateViewController(withIdentifier: "WalkThrough_ViewController") as! WalkThrough_ViewController
                    self.present(signIn_VC, animated: true, completion: nil)
                }
            }
        }
    }
    
    //    MARK:- Finish
}
