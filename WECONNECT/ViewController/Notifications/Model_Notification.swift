//
//  Model_Plan_Packages.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 24/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire

class Model_Notification {
    static let shared = Model_Notification()
    
    var arr_get_all_notifications = [Model_Notification]()
    
    var title = ""
    var time = ""
    var date = ""
    var vendor_profile = ""
    var vendor_name = ""
    
    var str_message = ""
    
    func func_get_notification_list(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_notification_list
        let str_param = "id=\(Model_Splash.shared.custumer_ID)&rec_type=customer"
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_param) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                
                self.arr_get_all_notifications.removeAll()
                let arr_Results = dict_JSON["result"] as! [[String:Any]]
                
                for dict in arr_Results {
                    self.arr_get_all_notifications.append(self.func_set_data(dict: dict))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    
    
    private func func_set_data(dict:[String:Any]) -> Model_Notification {
        let model = Model_Notification()
        let str_title = dict["title"] as! String
        let str_vendor_name = dict["vendor_name"] as! String
        
        model.title = "Hello, \(str_title) by \(str_vendor_name)"
        model.time = dict["time"] as! String
        model.date = dict["date"] as! String
        model.vendor_profile = dict["vendor_profile"] as! String
        
        return model
    }
    
    
}



