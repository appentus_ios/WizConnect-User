//
//  Chats_TableViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Chats_List_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var view_container:UIView!
    @IBOutlet weak var img_user:UIImageView!
    
    @IBOutlet weak var lbl_name:UILabel!
    @IBOutlet weak var lbl_msg:UILabel!
    @IBOutlet weak var lbl_time:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        img_user.layer.cornerRadius = img_user.frame.size.height/2
        img_user.clipsToBounds = true
        
        view_container.layer.cornerRadius = 2
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_container.layer.shadowRadius = 3.0
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    
}
