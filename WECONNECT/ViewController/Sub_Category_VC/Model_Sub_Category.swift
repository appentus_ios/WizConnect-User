//
//  Model_Category.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation


class Model_Sub_Category {
    
    static let shared = Model_Sub_Category()
    
    var sub_cate_id  = ""
    var sub_cate_name = ""
    var sub_cate_code = ""
    
    var category_code = ""
    var category_id   = ""
    var category_name = ""
    var category_icon = ""
    
    var arr_sub_category_list = [Model_Sub_Category]()
    
    func func_get_category(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_subcategory
        let str_param = "cate_code=\(Model_Categories.shared.category_code)"
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_param) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                
                let arr_Result = dict_JSON["result"] as? [[String:Any]]
                self.arr_sub_category_list.removeAll()
                for dict_result in arr_Result! {
                    self.arr_sub_category_list.append(self.func_set_category_Data(dict: dict_result))
                }
                
            }
            completionHandler(dict_JSON["status"] as! String)
        }
    }
    
    
    private func func_set_category_Data(dict:[String:Any]) -> Model_Sub_Category {
        let model = Model_Sub_Category()
        
        model.sub_cate_id = dict["sub_cate_id"] as! String
        model.sub_cate_name = dict["sub_cate_name"] as! String
        model.sub_cate_code = dict["sub_cate_code"] as! String
        
        model.category_id = dict["category_id"] as! String
        model.category_name = dict["category_name"] as! String
        model.category_icon = dict["category_icon"] as! String
        model.category_code = dict["category_code"] as! String
        
        return model
    }
    
    
}


