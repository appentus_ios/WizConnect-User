//
//  Category_TableViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Sub_Category_TableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_sub_category_name:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
