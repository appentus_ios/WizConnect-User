//
//  Category_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import ASStarRatingView


class Sub_Category_ViewController: UIViewController {

    @IBOutlet weak var tbl_subCategory:UITableView!
    
    @IBOutlet weak var txt_search:UITextField!
    @IBOutlet weak var btn_search:UIButton!
    
    var arr_searched = [Model_Sub_Category]()
    var arr_for_search = [Model_Sub_Category]()
    
    var label = UILabel()
    var str_slider_value = "5"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_search.layer.cornerRadius = 3
        txt_search.clipsToBounds = true
    }

    override func viewWillAppear(_ animated: Bool) {
        txt_search.isHidden = true
        btn_search.isSelected = false
        txt_search.text = ""
        
        Model_Sub_Category.shared.arr_sub_category_list.removeAll()
        
        func_ShowHud()
        Model_Sub_Category.shared.func_get_category { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                self.arr_for_search=Model_Sub_Category.shared.arr_sub_category_list
                self.tbl_subCategory.reloadData()
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    

    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_Search(_ sender: UIButton) {
        btn_search.isSelected = !(btn_search.isSelected)
        
        if txt_search.isHidden {
            txt_search.isHidden = false
        } else {
            txt_search.isHidden = true
            txt_search.text = ""
            Model_Sub_Category.shared.arr_sub_category_list = arr_for_search
        }
        
    }
    
    @IBAction func txt_Search(_ sender: UITextField) {
        arr_searched = [Model_Sub_Category]()
        
        for i in 0..<arr_for_search.count {
            let model = arr_for_search[i]
            let target = model.sub_cate_name
            if ((target as NSString?)?.range(of:txt_search.text!, options: .caseInsensitive))?.location == NSNotFound
            { } else {
                arr_searched.append(model)
            }
        }
        
        if (txt_search.text! == "") {
            arr_searched = arr_for_search
        }
        
        Model_Sub_Category.shared.arr_sub_category_list = arr_searched
//        if Model_Sub_Category.shared.arr_sub_category_list.count == 0 {
//            tbl_subCategory.isHidden = true
//        } else {
//            tbl_subCategory.isHidden = false
//        }
        
        tbl_subCategory.reloadData()
    }
    
    
}



extension Sub_Category_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
              return 260
        } else {
            return 60
        }
      
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Sub_Category.shared.arr_sub_category_list.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath)
            
             let slider = cell.viewWithTag(12) as? UISlider
            slider?.value = Float(str_slider_value)!
            slider?.addTarget(self, action: #selector(sliderValueChanged(sender:)), for: .valueChanged)
            
            let lbl_slider_value = cell.viewWithTag(11) as? UILabel
            lbl_slider_value?.text = str_slider_value
            
            let img_subCategoryIcon = cell.viewWithTag(1) as? UIImageView
            img_subCategoryIcon?.layer.cornerRadius = 3
            img_subCategoryIcon?.clipsToBounds = true
            
            img_subCategoryIcon!.sd_setShowActivityIndicatorView(true)
            img_subCategoryIcon!.sd_setIndicatorStyle(.gray)
            img_subCategoryIcon!.sd_setImage(with:Model_Categories.shared.category_icon, placeholderImage:(UIImage(named:"beauty.png")))
            
            img_subCategoryIcon?.layer.cornerRadius = 3
            img_subCategoryIcon?.clipsToBounds = true
            
            let lbl_subCategory_name = cell.viewWithTag(2) as? UILabel
            lbl_subCategory_name?.text = Model_Categories.shared.category_name
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath) as! Sub_Category_TableViewCell
            
            let model = Model_Sub_Category.shared.arr_sub_category_list[indexPath.row-1]
            cell.lbl_sub_category_name.text = model.sub_cate_name
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row > 0 {
            let model = Model_Sub_Category.shared.arr_sub_category_list[indexPath.row-1]
            Model_Sub_Category.shared.sub_cate_code = model.sub_cate_code
            Model_Sub_Category.shared.sub_cate_name = model.sub_cate_name
            
            let category_VC = storyboard?.instantiateViewController(withIdentifier: "Vender_list_ViewController") as! Vender_list_ViewController
            present(category_VC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func sliderValueChanged(sender: UISlider) {
        let currentValue = Int(sender.value)
        Model_Vender_list.shared.range = "\(currentValue)"
        
        str_slider_value = "\(currentValue)"
        
        let index = IndexPath (row: 0, section: 0)
        tbl_subCategory.reloadRows(at: [index], with: .none)
    }

    func updateLabels(nV: Float?) {
        if let v = nV {
            self.label.text = "\(v)"
        }
    }
    
}

