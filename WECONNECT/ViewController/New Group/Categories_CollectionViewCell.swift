
//  Categories_CollectionViewCell.swift
//  WECONNECT

//  Created by iOS-Appentus on 19/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.


import UIKit

class Categories_CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var view_container:UIView!
    
    @IBOutlet weak var img_cate_icon:UIImageView!
    @IBOutlet weak var lbl_cate_name:UILabel!
    
    override func awakeFromNib() {
        
        view_container.layer.cornerRadius = 4
        view_container.layer.shadowOpacity = 3.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_container.layer.shadowRadius = 2.0
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
}
