//
//  Model-.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation


class Model_Categories {
    
    static let shared = Model_Categories()
    
    var category_id = ""
    var category_name = ""
    var category_icon = URL (string: "")
    var category_code = ""
    
    var arr_category_list = [Model_Categories]()
    
    func func_get_category(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+K_get_category
        
        API_WizConnect.func_API_Call_GET(apiName: str_FullURL) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                let arr_Result = dict_JSON["result"] as? [[String:Any]]
                for dict_result in arr_Result! {
                    self.arr_category_list.append(self.func_set_category_Data(dict: dict_result))
                }
            }
            completionHandler(dict_JSON["status"] as! String)
        }
    }
    
    
   private func func_set_category_Data(dict:[String:Any]) -> Model_Categories {
        let model = Model_Categories()
        
        model.category_id = dict["category_id"] as! String
        model.category_name = dict["category_name"] as! String
        let str_category_icon = dict["category_icon"] as! String
        model.category_code = dict["category_code"] as! String
        
        if let encoded =  str_category_icon.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
        let url = URL(string: encoded) {
            model.category_icon = url
        }
    
        return model
    }

    
}






