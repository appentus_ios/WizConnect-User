//
//  Categories_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 19/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//





import UIKit
import SDWebImage
import CoreLocation
import GoogleMaps



class Categories_ViewController: UIViewController {
    @IBOutlet weak var view_searchForservice:UIView!
    @IBOutlet weak var lbl_locationForService:UILabel!
    
    @IBOutlet weak var coll_Category:UICollectionView!
    
    @IBOutlet weak var txt_search:UITextField!
    
    var arr_searched = [Model_Categories]()
    var arr_for_search = [Model_Categories]()
    
    var locationManager = CLLocationManager()
    
    var co_OrdinateCurrent = CLLocationCoordinate2DMake(0.0, 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_Send_Location), name:Notification.Name(rawValue: "send_Location_Chat"), object: nil)
        
        func_shadow(view_shadow: view_searchForservice)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        func_get_category()
    }
    
    func func_get_category() {
        is_from_jobDetails = false
        
        Model_Categories.shared.arr_category_list.removeAll()
        
        func_ShowHud()
        Model_Categories.shared.func_get_category { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                self.arr_for_search.removeAll()
                self.arr_for_search = Model_Categories.shared.arr_category_list
                self.coll_Category.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func func_shadow(view_shadow:UIView) {
        view_shadow.layer.cornerRadius = 4
        view_shadow.layer.shadowOpacity = 3.0
        view_shadow.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_shadow.layer.shadowRadius = 3.0
        view_shadow.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    func func_circle(img_round:UIImageView) {
        img_round.layer.cornerRadius = img_round.frame.size.height/2
        img_round.clipsToBounds = true
    }
    
    @IBAction func btn_search(_ sender:UIButton) {
        let map_VC = storyboard?.instantiateViewController(withIdentifier: "Map_ViewController") as! Map_ViewController
        present(map_VC, animated: true, completion: nil)
    }
    
    
    @objc func func_Send_Location() {
        lbl_locationForService.text = str_selectYourLocations
        func_get_category()
    }
    
    @IBAction func txt_Search(_ sender: UITextField) {
        arr_searched = [Model_Categories]()
        
        for i in 0..<arr_for_search.count {
            let model = arr_for_search[i]
            let target = model.category_name
            if ((target as NSString?)?.range(of:txt_search.text!, options: .caseInsensitive))?.location == NSNotFound
            { } else {
                arr_searched.append(model)
            }
        }
        if (txt_search.text! == "") {
            arr_searched = arr_for_search
        }
        Model_Categories.shared.arr_category_list = arr_searched
        coll_Category.reloadData()
    }
    
}



//  MARK:- UICollectionView methods
extension Categories_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: collectionView.frame.size.width/3-1.5, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Model_Categories.shared.arr_category_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let coll_Car = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Categories_CollectionViewCell
        
        let model = Model_Categories.shared.arr_category_list[indexPath.row]
        
        coll_Car.lbl_cate_name.text = model.category_name
        
        coll_Car.img_cate_icon.sd_setShowActivityIndicatorView(true)
        coll_Car.img_cate_icon.sd_setIndicatorStyle(.gray)
        
        coll_Car.img_cate_icon.sd_setImage(with:model.category_icon, placeholderImage:(UIImage(named:"beauty.png")))
        
        return coll_Car
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = Model_Categories.shared.arr_category_list[indexPath.row]

        Model_Categories.shared.category_name = model.category_name
        Model_Categories.shared.category_icon = model.category_icon
        Model_Categories.shared.category_code = model.category_code
        
        let category_VC = storyboard?.instantiateViewController(withIdentifier: "Sub_Category_ViewController") as! Sub_Category_ViewController
        present(category_VC, animated: true, completion: nil)
    }
    

}



extension Categories_ViewController:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        co_OrdinateCurrent = manager.location!.coordinate
        co_Ordinate_selected = manager.location!.coordinate
        
        func_Geocoder(co_OrdinateCurrent) { (status) in
            self.lbl_locationForService.text = status
        }
        
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func func_Geocoder(_ coordinate: CLLocationCoordinate2D,completion:@escaping (String)->()) {
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            let str_Address = lines.joined(separator: "\n")
            print(str_Address)
            
            completion(str_Address)
        }
    }
    
}





