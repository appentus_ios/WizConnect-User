
//
//  Add_New_TableViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 19/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Add_New_TableViewCell: UITableViewCell {
        
    @IBOutlet weak var view_container: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_container.layer.shadowRadius = 1.5
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    
}
