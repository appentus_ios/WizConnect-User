//
//  Model_SignIn.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire



class Model_Chat {
    static let shared = Model_Chat()
    
    var fri_name = ""
    var fri_img = ""
    
    var user_id = ""
    var fri_id = ""
    
    var date = ""
    var time = ""
    var msg = ""
    
    var str_msg_response = ""
    
    var arr_chat_list = [Model_Chat]()
    
    func func_chat_msg(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL_chat+k_chat_msg
        
        let str_Params = [
            "user_id":Model_Splash.shared.custumer_ID,
            "fri_id":"\(fri_id)"
        ]
        print(str_Params)
        
        API_WizConnect.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.arr_chat_list.removeAll()
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_chat_list.append(self.func_set_chat_data(dict: dict_json))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    private func func_set_chat_data(dict:[String:Any]) -> Model_Chat {
        let model = Model_Chat()
        
        model.date = dict["date"] as! String
        model.msg = dict["msg"] as! String
        model.time = dict["time"] as! String
        model.user_id = dict["user_id"] as! String
        model.fri_id = dict["fri_id"] as! String
        
        return model
    }
    
    var msg_send = ""
    
    func func_insert_chat(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL_chat+k_insert_chat
        
        let str_Params = [
            "user_id":Model_Splash.shared.custumer_ID,
            "fri_id":"\(fri_id)",
            "msg":"\(msg_send)",
            "type":"1",
            "receiver":"2",
            "fri_id_type":"vendor",
            "user_id_type": "customer",
        ]
        print(str_Params)
        
        API_WizConnect.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_msg_response = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    
    
}



