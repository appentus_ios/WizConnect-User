//
//  AppDelegate.swift
//  WECONNECT
//  Created by Raja Vikram singh on 22/11/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.


import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

import Firebase
import FirebaseMessaging
import Crashlytics

import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

import UserNotifications

var k_FireBaseFCMToken = ""
//let k_GOOLGE_API_KEY = "AIzaSyDPLCKl_WWGdD04XZC0YX95hBlWtthEYmU"
let k_GOOLGE_API_KEY = "AIzaSyA80-ommi_fpf76xQLUduhno1XQG1uVix0"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,MessagingDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Fabric.sharedSDK().debug = true
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")

            }
        }
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        GIDSignIn.sharedInstance().clientID = "90681053308-d4s8qf6e6br4s5fptg30jdjrup32m7di.apps.googleusercontent.com"
        IQKeyboardManager.shared.enable = true
        
        GMSServices.provideAPIKey(k_GOOLGE_API_KEY)
        GMSPlacesClient.provideAPIKey(k_GOOLGE_API_KEY)
        
        askForNotificationPermission(application)

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        k_FireBaseFCMToken = fcmToken
    }
    
    
    
}





extension AppDelegate:UNUserNotificationCenterDelegate {
    func askForNotificationPermission(_ application: UIApplication) {
        let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
        if !isRegisteredForRemoteNotifications {
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                    (granted, error) in
                    if error == nil {
                        if !granted {
                            self.openNotificationInSettings()
                        } else {
                            UNUserNotificationCenter.current().delegate = self
                        }
                    }
                }
            } else {
                
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                application.registerUserNotificationSettings(settings)
            }
        } else {
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().delegate = self
            } else {
                
            }
        }
        
        application.registerForRemoteNotifications()
        
    }
    
    func openNotificationInSettings() {
        let alertController = UIAlertController(title: "Notification Alert", message: "Please enable Notification from Settings to never miss a text.", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    })
                } else {
                    
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        DispatchQueue.main.async {
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .badge, .sound])
        
        let dataDict = notification.request.content.userInfo
        
        let dict_title = dataDict["aps"] as! [String:Any]
        let dict_alert = dict_title["alert"] as! [String:Any]
        let str_title = dict_alert["title"] as! String
        print(str_title)
        
        let result : String = dataDict["result"] as! String
//        let jsonResp : [String:Any] = anyConvertToDictionary(text: result)!
        
        if str_title == "chat" {
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                NotificationCenter.default.post(name: Notification.Name("chat_incoming"), object: nil)
            }
        } else if str_title == "Request Accept By Vendor" || str_title == "Your Request In Progress" || str_title == "Request Cancel By Vendor" {
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                NotificationCenter.default.post(name: Notification.Name("reload_req"), object: nil)
            }
        } else if str_title == "Your request is completed" {
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                NotificationCenter.default.post(name: Notification.Name("complete_req"), object: nil)
            }
        }
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let dataDict = userInfo
        let dict_title = dataDict["aps"] as! [String:Any]
        let dict_alert = dict_title["alert"] as! [String:Any]
        let str_title = dict_alert["title"] as! String
        print(str_title)
        
        let result : String = dataDict["result"] as! String
        let jsonResp : [String:Any] = anyConvertToDictionary(text: result)!
        print(jsonResp)
        
        if str_title == "chat" {
            let dict_data = jsonResp["data"] as! [String:Any]
            Model_Chat.shared.fri_id = dict_data["user_id"] as! String
            
            func_Set_Action_On_Getting_Notifications(jsonResp: jsonResp)
            
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                NotificationCenter.default.post(name: Notification.Name("chat_incoming"), object: nil)
            }
        }
        
    }
    
    func func_Set_Action_On_Getting_Notifications(jsonResp:[String:Any]) {
        
        let appdelgateObj = UIApplication.shared.delegate as! AppDelegate
        
        let storyboard = UIStoryboard (name: "Main", bundle: nil)
        
        if let destinationVC = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
            
            if let window = appdelgateObj.window , let rootViewController = window.rootViewController {
                var currentController = rootViewController
                
                while let presentedController = currentController.presentedViewController {
                    currentController = presentedController
                }
                
                if currentController == self.window!.visibleViewController {
                    print(currentController.restorationIdentifier,self.window?.visibleViewController?.restorationIdentifier)
                    print("same view controller")
                } else {
                    currentController.present(destinationVC, animated: true, completion: nil)
                }
                
            }
        }
    }
    
    
}



public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(pvc)
            } else {
                return vc
            }
        }
    }
}



