//
//  Map_ViewController.swift
//  TacTec
//
//  Created by Raja Vikram singh on 21/08/18.
//  Copyright © 2018 Appentus. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

var co_Ordinate_selected = CLLocationCoordinate2DMake(0.0, 0.0)


class Map_ViewController: UIViewController  {
    //    MARK:- IBOutlets
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var lbl_Location: UILabel!
    @IBOutlet weak var btn_confirmLocation: UIButton!
//    @IBOutlet weak var lbl_search: UILabel!

    @IBOutlet weak var navbar: UINavigationItem!

    
    //    MARK:- vars
    var locationManager = CLLocationManager()
    
    var co_OrdinateCurrent = CLLocationCoordinate2DMake(0.0, 0.0)
    
    var marker_SelectedLocation = GMSMarker()
    
    
    
    //    MARK:- VC's life Cycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }

        mapView.isMyLocationEnabled = true
    
        
        btn_confirmLocation.layer.cornerRadius = btn_confirmLocation.frame.size.height/2
//        btn_confirmLocation.layer.borderColor = UIColor .lightGray.cgColor
//        btn_confirmLocation.layer.borderWidth = 1
        btn_confirmLocation.clipsToBounds = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    MARK:- IBActions
    @IBAction func btn_SearchLocation(_ sender:Any)  {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func btn_back(_ sender:Any)  {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func btn_Done(_ sender:Any)  {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            NotificationCenter.default.post(name: Notification.Name("send_Location_Chat"), object: nil)
        }
        
        dismiss(animated: true, completion: nil)
    }

    
    //    MARK:- Custom functions
    
    //    MARK:- Finish
}
