//
//  File.swift
//  TacTec
//
//  Created by Raja Vikram singh on 21/08/18.
//  Copyright © 2018 Appentus. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces
import CoreLocation


extension Map_ViewController:GMSMapViewDelegate {
    func set_GoogleMap() {
        
        let position = CLLocationCoordinate2D(latitude:co_OrdinateCurrent.latitude, longitude:co_OrdinateCurrent.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 15)
        
        mapView.camera = camera
        mapView?.animate(to: camera)
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print(coordinate)
        func_ShowMarker(cordinate: coordinate)
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        
        func_ShowMarker(cordinate: (mapView.myLocation?.coordinate)!)

        return true
    }
}


extension Map_ViewController:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        co_OrdinateCurrent = manager.location!.coordinate
        set_GoogleMap()
        func_ShowMarker(cordinate: co_OrdinateCurrent)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

//        MARK:- GMSAutocompleteView methods
extension Map_ViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        str_selectYourLocations = place.formattedAddress ?? "address not found"
        lbl_Location.text = str_selectYourLocations
        func_ShowMarker(cordinate: place.coordinate)
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
}



extension Map_ViewController {
    func func_ShowMarker(cordinate:CLLocationCoordinate2D) {
        if marker_SelectedLocation != nil {
            mapView.clear()
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: cordinate.latitude, longitude:cordinate.longitude, zoom: 15)
        
        mapView.camera = camera
        mapView?.animate(to: camera)
        
        marker_SelectedLocation = GMSMarker(position: cordinate)
        marker_SelectedLocation.map = mapView
        marker_SelectedLocation.icon = UIImage (named:"Map-Marker-Pink-icon.png")
        
        co_Ordinate_selected = cordinate
        
        func_Geocoder(cordinate) { (str_Address) in
            DispatchQueue.main.async {
                self.lbl_Location.text = str_Address
                str_selectYourLocations=str_Address
            }
        }
    }
}


extension Map_ViewController {
    func func_Geocoder(_ coordinate: CLLocationCoordinate2D,completion:@escaping (String)->()) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            let str_Address = lines.joined(separator: "\n")
            print(str_Address)
            
            completion(str_Address)
        }
    }
}


