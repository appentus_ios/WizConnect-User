//
//  Model_Job_details.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 03/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//



import Foundation
import Alamofire



class Model_Job_details {
    static let shared = Model_Job_details()
    
    var booking_id = ""
    var str_message = ""
    
    var receiver_id = ""
    var comment = ""
    var rating = ""
    
    func func_cancel(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_cancel_request_by_customer
        let str_params = "booking_id=\(booking_id)"

        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_message = dict_JSON["message"] as! String
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    func func_do_feedback(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_do_feedback
        let str_params = "sender_id=\(Model_Splash.shared.custumer_ID)&receiver_id=\(receiver_id)&comment=\(comment)&rating=\(rating)"
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_message = dict_JSON["message"] as! String
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    

    
    
//    func func_accept_request(completionHandler:@escaping (String)->()) {
//        let str_FullURL = k_BaseURL+k_accept_request
//        let str_params = "booking_id=\(booking_id)"
//
//        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
//            (dict_JSON) in
//            print(dict_JSON)
//
//            if dict_JSON["status"] as? String == "success" {
//                self.str_message = dict_JSON["message"] as! String
//                completionHandler(dict_JSON["status"] as! String)
//            } else {
//                if let str_status = dict_JSON["status"] as? String {
//                    if str_status == "failed" {
//                        self.str_message = dict_JSON["message"] as! String
//                        completionHandler(dict_JSON["status"] as! String)
//                    } else {
//                        completionHandler("false")
//                    }
//                } else {
//                    completionHandler("false")
//                }
//            }
//        }
//    }
//
//    func func_request_inprogress_vendor(completionHandler:@escaping (String)->()) {
//        let str_FullURL = k_BaseURL+K_request_inprogress_vendor
//        let str_params = "booking_id=\(booking_id)"
//
//        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
//            (dict_JSON) in
//            print(dict_JSON)
//
//            if dict_JSON["status"] as? String == "success" {
//                self.str_message = dict_JSON["message"] as! String
//                completionHandler(dict_JSON["status"] as! String)
//            } else {
//                if let str_status = dict_JSON["status"] as? String {
//                    if str_status == "failed" {
//                        self.str_message = dict_JSON["message"] as! String
//                        completionHandler(dict_JSON["status"] as! String)
//                    } else {
//                        completionHandler("false")
//                    }
//                } else {
//                    completionHandler("false")
//                }
//            }
//        }
//    }
//
//
//
//    func func_request_complete_vendor(completionHandler:@escaping (String)->()) {
//        let str_FullURL = k_BaseURL+k_request_complete_vendor
//        let str_params = "booking_id=\(booking_id)"
//
//        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
//            (dict_JSON) in
//            print(dict_JSON)
//
//            if dict_JSON["status"] as? String == "success" {
//                self.str_message = dict_JSON["message"] as! String
//                completionHandler(dict_JSON["status"] as! String)
//            } else {
//                if let str_status = dict_JSON["status"] as? String {
//                    if str_status == "failed" {
//                        self.str_message = dict_JSON["message"] as! String
//                        completionHandler(dict_JSON["status"] as! String)
//                    } else {
//                        completionHandler("false")
//                    }
//                } else {
//                    completionHandler("false")
//                }
//            }
//        }
//    }
    
}


