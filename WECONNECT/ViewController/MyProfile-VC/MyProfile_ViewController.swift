//
//  MyProfile_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 18/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import CountryPicker
import ACFloatingTextfield_Swift


class MyProfile_ViewController: UIViewController,CountryPickerDelegate,UITextFieldDelegate {
    @IBOutlet weak var btn_CameraIcon:UIButton!
    @IBOutlet weak var img_Profile:UIImageView!
    
    @IBOutlet weak var txt_name:ACFloatingTextfield!
    @IBOutlet weak var txt_email:ACFloatingTextfield!
    @IBOutlet weak var txt_mobile:ACFloatingTextfield!
    
    @IBOutlet weak var picker: CountryPicker!
    @IBOutlet weak var view_CountryPicker: UIView!
    
    @IBOutlet weak var view_1:UIView!
    
    @IBOutlet weak var btn_UpdateInfo:UIButton!
    @IBOutlet weak var btn_CountryCode: UIButton!
    
    @IBOutlet weak var hieght_otp_view: NSLayoutConstraint!
//    @IBOutlet weak var hieght_all_view: NSLayoutConstraint!
//    @IBOutlet weak var hieght_profile_view: NSLayoutConstraint!
    
    @IBOutlet weak var view_otp: UIView!
    
    @IBOutlet weak var lbl_Timer:UILabel!
    @IBOutlet weak var btn_send_OTP:UIButton!
    
    @IBOutlet weak var txt_OTP:UITextField!
    
    var timer_count = 30
    var gameTimer: Timer!
    var str_selected_cate_code = ""
    var str_new_OTP = ""
    var str_old_OTP = ""
    var str_old_Country_code = ""
    var str_old_mobile_number = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        func_AddCountryPicker()
        
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            print(dict_LoginData)
            let str_img = dict_LoginData["customer_profile"] as! String
            
            img_Profile.sd_setShowActivityIndicatorView(true)
            img_Profile.sd_setIndicatorStyle(.gray)
            img_Profile.sd_setImage(with:URL (string: str_img), placeholderImage:(UIImage(named:"user-gray.png")))
            
            txt_name.text = dict_LoginData["customer_name"] as? String
            txt_email.text = dict_LoginData["customer_email"] as? String
            
            btn_CountryCode.setTitle("\(dict_LoginData["customer_country_code"]!)", for: .normal)
            str_old_Country_code = btn_CountryCode.currentTitle!
            
            txt_mobile.text = dict_LoginData["customer_mobile"] as? String
            str_old_mobile_number = txt_mobile.text!
            
            txt_OTP.text = dict_LoginData["customer_otp"] as? String
            str_new_OTP = txt_OTP.text!
        }
        
        txt_OTP.layer.borderColor = UIColor .lightGray.cgColor
        txt_OTP.layer.borderWidth = 1
        
        hieght_otp_view.constant = 0
        view_otp.isHidden = true
        
        view_CountryPicker.isHidden = true
        
        btn_CameraIcon.layer.cornerRadius = btn_CameraIcon.frame.size.height/2
        btn_CameraIcon.clipsToBounds = true
        
        img_Profile.layer.cornerRadius = img_Profile.frame.size.height/2
        img_Profile.clipsToBounds = true
        
        btn_UpdateInfo.layer.cornerRadius = btn_UpdateInfo.frame.size.height/2
        btn_UpdateInfo.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_update (_ sender:Any) {
        if !func_Validation() {
            
            return
        }
        
        func_ShowHud()
        Model_Account.shared.customer_img = img_Profile.image!
        Model_Account.shared.customer_name = txt_name.text!
        Model_Account.shared.customer_email = txt_email.text!
        Model_Account.shared.customer_country_code = btn_CountryCode.currentTitle!
        Model_Account.shared.customer_mobile = txt_mobile.text!
        
        Model_Account.shared.func_update_profile_customer { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Account.shared.str_message)
                } else {
                    self.func_ShowHud_Error(with: Model_Account.shared.str_message)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                    self.func_HideHud()
                })
            }
        }
    }
    
    @IBAction func btn_select_image (_ sender:Any) {
        func_ChooseImage()
    }
    
    @IBAction func btn_back (_ sender:Any) {
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "update_Ac"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    func func_AddCountryPicker() {
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //        picker.displayOnlyCountriesWithCodes = ["DK", "SE", "NO", "DE"] //display only
        //        picker.exeptCountriesWithCodes = ["RU"] //exept country
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if txt_mobile == textField {
//            if txt_mobile.text == str_old_mobile_number {
//                if btn_CountryCode.currentTitle == str_old_Country_code {
//                    str_new_OTP = str_old_OTP
//
//                    hieght_otp_view.constant = 0
////                    hieght_all_view.constant = 880
////                    hieght_profile_view.constant = 470
//                    view_otp.isHidden = true
//                } else {
//                    str_new_OTP = "-1"
//
//                    hieght_otp_view.constant = 80
////                    hieght_all_view.constant = 960
////                    hieght_profile_view.constant = 550
//                    view_otp.isHidden = false
//                }
//            } else {
//                str_new_OTP = "-1"
//
//                hieght_otp_view.constant = 80
////                hieght_all_view.constant = 960
////                hieght_profile_view.constant = 550
//                view_otp.isHidden = false
//            }
//        }
        
    }
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        btn_CountryCode.setTitle(phoneCode, for:.normal)
        
//        if btn_CountryCode.currentTitle == str_old_Country_code {
//            if txt_mobile.text == str_old_mobile_number {
//                str_new_OTP = str_old_OTP
//
//                hieght_otp_view.constant = 0
////                hieght_all_view.constant = 880
////                hieght_profile_view.constant = 470
//                view_otp.isHidden = true
//            } else {
//                str_new_OTP = "-1"
//
//                hieght_otp_view.constant = 80
////                hieght_profile_view.constant = 550
////                hieght_all_view.constant = 960
//                view_otp.isHidden = false
//            }
//        } else {
//            str_new_OTP = "-1"
//
//            hieght_otp_view.constant = 80
////            hieght_profile_view.constant = 550
////            hieght_all_view.constant = 960
//            view_otp.isHidden = false
//        }
    }
    
    @IBAction func btn_sendOTP(_ sender:Any) {
        
        Model_SignUp.shared.c_code = btn_CountryCode.currentTitle!
        Model_SignUp.shared.customer_email = txt_email.text!
        Model_SignUp.shared.customer_mobile = txt_mobile.text!
        
        func_ShowHud()
        Model_SignUp.shared.func_SendOTP { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status != "success" {
                    self.func_ShowHud_Success(with: Model_SignUp.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                    })
                } else if status == "success" {
                    self.str_new_OTP = Model_SignUp.shared.str_OTP
                    
                    self.btn_send_OTP.setTitle("Resend OTP", for: .normal)
                    self.gameTimer = Timer.scheduledTimer(timeInterval:1, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
                    self.timer_count = 30
                    
                    self.func_ShowHud_Success(with: Model_SignUp.shared.str_OTP)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
    @objc func runTimedCode() {
        timer_count = timer_count-1
        
        lbl_Timer.text = "\(timer_count)"
        
        if timer_count == 0 {
            gameTimer.invalidate()
            btn_send_OTP.setTitle("Resend OTP", for: .normal)
        }
        
    }

    
    @IBAction func btn_country_code(_ sender: UIButton) {
        view_CountryPicker.isHidden = false
    }
    
    @IBAction func btn_DoneCoutrnyPicker(_ sender: UIButton) {
        view_CountryPicker.isHidden = true
    }
    
    @IBAction func btn_CountryCode(_ sender: UIButton) {
        view_CountryPicker.isHidden = false
        self.view.endEditing(true)
    }
    
    
    func func_Validation() -> Bool {
        let is_Email = func_IsValidEmail(testStr: txt_email.text!)
        
        if img_Profile.image == UIImage (named: "user-gray.png") {
            func_ShowHud_Error(with: "Select image")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        } else if txt_name.text!.isEmpty {
            func_ShowHud_Error(with: "Enter your name")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        } else if txt_email.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Email")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        } else if !is_Email {
            func_ShowHud_Error(with: "Enter valid Email")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        } else if txt_mobile.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Phone number")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1) {
                self.func_HideHud()
            }
            return false
        }
//        else if btn_CountryCode.currentTitle != str_old_Country_code || txt_mobile.text != str_old_mobile_number {
//            if txt_OTP.text != str_new_OTP {
//                func_ShowHud_Error(with: "OTP is not matched")
//
//                DispatchQueue.main .asyncAfter(deadline:.now()+1.5) {
//                    self.func_HideHud()
//                }
//                return false
//            } else {
//                return true
//            }
//        }
        else {
            return true
        }
    }
    
}



extension MyProfile_ViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func func_ChooseImage() {
        
        let alert = UIAlertController(title: "", message: "Please select", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenCamera()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Photos", style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenGallary()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func func_OpenCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate=self
            
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera in simulator", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func func_OpenGallary()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate=self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            img_Profile.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
}






