//
//  Model_Job_details.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 03/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//



import UIKit
import Foundation
import Alamofire
import SVProgressHUD


class Model_Book {
    static let shared = Model_Book()
    
    var str_message = ""
    
    var vendor_works_id = ""
    var vendor_works_image = ""
    var works_vendor_id = ""
    
    var arr_all_documents = [Model_Book]()
    
    func func_get_about(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_about
        let str_params = "vendor_id=\(Model_Vender_list.shared.vendor_id)"
        print(str_params)
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                
                self.arr_all_documents.removeAll()
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_all_documents.append(self.func_setdata(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    
    private func func_setdata(dict:[String:Any]) -> Model_Book {
        let model = Model_Book()
        
        model.vendor_works_id = "\(dict["vendor_works_id"] ?? "")"
        model.vendor_works_image = "\(dict["vendor_works_image"] ?? "")"
        model.works_vendor_id = "\(dict["works_vendor_id"] ?? "")"
        
        return model
    }
    
}


