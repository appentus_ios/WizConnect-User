//
//  Book_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 19/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import SDWebImage



class Book_ViewController: UIViewController {
    @IBOutlet weak var leading_downline:NSLayoutConstraint!
    
    @IBOutlet weak var img_user:UIImageView!
    @IBOutlet weak var lbl_vender_name:UILabel!
    @IBOutlet weak var lbl_cate_name:UILabel!
    @IBOutlet weak var lbl_distance:UILabel!
    @IBOutlet weak var lbl_vender_cost:UILabel!
    @IBOutlet weak var lbl_distance_1:UILabel!
    
    @IBOutlet weak var lbl_rating:UILabel!
    @IBOutlet weak var lbl_rating_avg:UILabel!
    
    @IBOutlet weak var lbl_about:UILabel!
    
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_11:UIView!
    
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var view_3:UIView!
    
    @IBOutlet weak var tbl_Reviews:UITableView!
    @IBOutlet weak var tbl_About:UITableView!
    
    @IBOutlet weak var view_About:UIView!
    @IBOutlet weak var lbl_no_review_yet:UILabel!
    @IBOutlet weak var btn_mobile_number:UIButton!
    
    @IBOutlet weak var btn_book_now:UIButton!
    @IBOutlet weak var btn_message:UIButton!
    
    var is_review = false
    
    var arr_sub_Cate_services = [String]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbl_Reviews.isHidden = true
        
        if is_from_jobDetails {
            btn_book_now.isHidden = is_from_jobDetails
            btn_message.isHidden = is_from_jobDetails
        } else {
            btn_book_now.isHidden = is_from_jobDetails
            btn_message.isHidden = is_from_jobDetails
        }
        
        img_user.sd_setShowActivityIndicatorView(true)
        img_user.sd_setIndicatorStyle(.gray)
        img_user.sd_setImage(with:URL (string:Model_Vender_list.shared.vendor_profile), placeholderImage:(UIImage(named:"user-gray.png")))
        
        lbl_vender_name.text = Model_Vender_list.shared.vendor_name
        lbl_cate_name.text = "| "+Model_Vender_list.shared.category_name
        lbl_distance.text = Model_Vender_list.shared.distance
        lbl_distance_1.text = Model_Vender_list.shared.distance
        lbl_vender_cost.text = Model_Vender_list.shared.vendor_cost
        lbl_rating.text = "("+Model_Vender_list.shared.rating+")"
        lbl_rating_avg.text = Model_Vender_list.shared.rating_avg
        lbl_about.text = Model_Vender_list.shared.vendor_about
        btn_mobile_number.setTitle("Call to vendor :- \(Model_Vender_list.shared.vendor_mobile)", for: .normal)
        
        func_shadow(view_shadow: view_1)
        func_shadow(view_shadow: view_11)
        
        img_user.layer.cornerRadius = img_user.frame.size.height/2
        img_user.clipsToBounds = true
        
        view_2.isHidden = false
        view_3.isHidden = true
        
        view_About.isHidden = false
        tbl_Reviews.isHidden = true
        
        func_get_subCate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        func_get_feedback()
        func_get_about()
    }
    
    
    func func_get_about()  {
        func_ShowHud()
        Model_Book.shared.func_get_about { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                self.tbl_About.reloadData()
            }
        }
    }
    
    func func_get_subCate() {
        let str_subcate_code = Model_Vender_list.shared.vendor_subcate_code
        print(str_subcate_code)
        let arr_subcate_code = str_subcate_code.components(separatedBy: ",")
        print(arr_subcate_code)
        
        arr_sub_Cate_services.removeAll()
        
        func_ShowHud()
        Model_Sub_Category.shared.func_get_category { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                for str_subcate_code_1 in arr_subcate_code {
                    for model in Model_Sub_Category.shared.arr_sub_category_list {
                        if str_subcate_code_1 == model.sub_cate_code {
                            self.arr_sub_Cate_services.append(model.sub_cate_name)
                        }
                    }
                }
                
                if arr_subcate_code.count == 0 {
                    self.tbl_About.isHidden = true
                } else {
                    self.tbl_About.isHidden = false
                }
                
                self.tbl_About.reloadData()
            }
        }
        
    }
    
    func func_get_feedback()  {
        func_ShowHud()
        Model_Reviews.shared.func_get_feedback { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                if status == "success" {
                    if Model_Reviews.shared.arr_feedbacks.count == 0 {
                        self.tbl_Reviews.isHidden = true
                        self.lbl_no_review_yet.isHidden = false
                    } else {
                        self.tbl_Reviews.isHidden = false
                        self.lbl_no_review_yet.isHidden = true
                    }
                } else {
                   
                }
                self.tbl_Reviews.reloadData()
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func func_shadow(view_shadow:UIView) {
        view_shadow.layer.cornerRadius = 1
        view_shadow.layer.shadowOpacity = 3.0
        view_shadow.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_shadow.layer.shadowRadius = 3.0
        view_shadow.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    @IBAction func btn_Message (_ sender:UIButton) {
        Model_Chat.shared.fri_id = Model_Vender_list.shared.vendor_id
        
        let chat_VC = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        present(chat_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_mobile_number (_ sender:UIButton) {
            self.func_call()
     }
    
    @IBAction func btn_Reviews (_ sender:UIButton) {
        leading_downline.constant = sender.frame.origin.x
        
        view_2.isHidden = true
        view_3.isHidden = false
        
        view_About.isHidden = true
        tbl_Reviews.isHidden = false
        
        is_review = true
        tbl_Reviews.reloadData()
    }
    
    @IBAction func btn_About (_ sender:UIButton) {
        leading_downline.constant = sender.frame.origin.y
        
        view_2.isHidden = false
        view_3.isHidden = true
        
        view_About.isHidden = false
        tbl_Reviews.isHidden = true
        is_review = false
        tbl_About.reloadData()
    }
    
    @IBAction func btn_Book_now (_ sender:Any) {
        let book_now_VC = storyboard?.instantiateViewController(withIdentifier: "Book_Now_ViewController") as! Book_Now_ViewController
        present(book_now_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_Back (_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }

    func func_call() {
        if let phoneCallURL = URL(string: "tel://\(Model_Vender_list.shared.vendor_mobile)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }

    
}


extension Book_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tbl_Reviews{
            return 120
        } else {
            if indexPath.row < arr_sub_Cate_services.count {
                return 30
            } else {
                return 200
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tbl_Reviews{
            return Model_Reviews.shared.arr_feedbacks.count
        } else {
            return arr_sub_Cate_services.count+Model_Book.shared.arr_all_documents.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tbl_Reviews{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_2", for: indexPath) as! Your_Reviews_TableViewCell
            
            let model = Model_Reviews.shared.arr_feedbacks[indexPath.row]
            
            cell.img_customer_profile.sd_setShowActivityIndicatorView(true)
            cell.img_customer_profile.sd_setIndicatorStyle(.gray)
            cell.img_customer_profile.sd_setImage(with:URL (string: model.customer_profile), placeholderImage:(UIImage(named:"user-gray.png")))
            
            cell.customer_name.text = model.customer_name
            cell.date.text = model.date
            cell.comment.text = model.comment
            cell.rating.text = model.rating
            
            cell.view_star.rating = Float(Int(model.rating)!)
            
            return cell
        }
        else{
            if indexPath.row < arr_sub_Cate_services.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell_1", for: indexPath)
                
                let lbl_sub_Cate_service = cell.viewWithTag(1) as! UILabel
                lbl_sub_Cate_service.text =  arr_sub_Cate_services[indexPath.row]
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath)
                
                let img_vendor_work = cell.viewWithTag(1) as! UIImageView
                
                let model = Model_Book.shared.arr_all_documents[indexPath.row-arr_sub_Cate_services.count]

                img_vendor_work.sd_setShowActivityIndicatorView(true)
                img_vendor_work.sd_setIndicatorStyle(.gray)
                img_vendor_work.sd_setImage(with:URL (string: model.vendor_works_image), placeholderImage:(UIImage(named:"document-2.png")))
                
                img_vendor_work.layer.borderColor = color_AppDefault  .cgColor
                img_vendor_work.layer.borderWidth = 1
                img_vendor_work.layer.cornerRadius = 2
                
                return cell
            }

         }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}


