//
//  WizConnect_TabBarController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

var tabBar_selectedIndex = 0

class WizConnect_TabBarController: UITabBarController , UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        
        guard
            let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else {
                return
        }
        statusBarView.backgroundColor = statusBarColor
        
        selectedIndex = 2
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
   override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    
    }
    
}


