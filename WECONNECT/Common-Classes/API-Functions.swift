//
//  API-Functions.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//


import Foundation
import Alamofire
import SVProgressHUD


class API_WizConnect {
    
    class func postAPI(url: String , parameters: [String:Any] , completion: @escaping ([String:Any]) -> ()) {
        let url = URL (string: url)
        
        Alamofire.request(url!, method: .post, parameters: parameters).validate().responseString(completionHandler: { (response) in
            
                let responseJson = anyConvertToDictionary(text: response.result.value ?? func_JSON_string())
                let dict_pure =  cleanJsonToObject(data: responseJson as AnyObject) as! [String:Any]
                completion(dict_pure)
        })
    }
    

    
    //    MARK:- apiCall_GET
    class func func_API_Call_GET(apiName:String,completionHandler:@escaping ([String:Any])->())
    {
        if Reachability.isConnectedToNetwork(){
            let url = URL (string: apiName)
            
            let request = NSMutableURLRequest (url:url!)
            print(request)
            
            let session = URLSession.shared
            request.httpMethod = "GET"
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            session.dataTask(with: request as URLRequest, completionHandler:
                {
                    (data, response, error) in
                    print("error:-",error ?? "error nil")
                    
                    if error == nil
                    {
                        do
                        {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                            let json_Clean = cleanJsonToObject(data: jsonData as AnyObject)
                            print(json_Clean)
                            
                            completionHandler(json_Clean as! [String:Any])
                        }
                        catch
                        {
                            print("Error info: \(error)")
                            completionHandler(["status":"false","message":"\(error)"])
                        }
                    }
                    else
                    {
                        completionHandler(["status":"false","message":"Error In API"])
                    }
            }).resume()
        } else {
            SVProgressHUD.showError(withStatus: "Internet Connection not Available!")
        }
    }
    

    class func func_API_Call_POST(str_URL:String,param:String,completionHandler:@escaping ([String:Any])->()) {
        if Reachability.isConnectedToNetwork() {
            
            let url = URL(string: str_URL)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            let dataaa =  param.data(using: .utf8)
            request.httpBody = dataaa
            request.timeoutInterval = 60
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                print(error ?? "error is nil")
                do {
                    if error == nil {
                        let json =  try JSONSerialization .jsonObject(with:data!
                            , options: .allowFragments)
                        let json_Clean = cleanJsonToObject(data: json as AnyObject)
                        
                        completionHandler(json_Clean as! [String:Any])
                    } else {
                        completionHandler(["status":"false","message":"\(error!)"])
                    }
                } catch let error as NSError {
                    print("error is:-",error)
                    completionHandler(["status":"false","message":"Error In API"])
                }
            }
            task.resume()
        }else{
            SVProgressHUD.showError(withStatus: "Internet Connection not Available!")
        }
    }
    
    
    
    class func func_UploadWithImage(endUrl: String,imageData: Data?, parameters: [String : String], completionHandler:@escaping ([String:Any])->())
    {
        if Reachability.isConnectedToNetwork() {
            
            let url = URL (string: endUrl) /* your API url */
            
            let headers: HTTPHeaders = [
                "Content-type": "multipart/form-data"
            ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if let data = imageData {
                        multipartFormData.append(data, withName: "file", fileName: "image.png", mimeType: "image/png")
                }
                
            }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
                
                print(result)
                
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do {
                            let json =  try JSONSerialization .jsonObject(with:response.data!
                                , options: .allowFragments)
                            print(json)
                            
                            completionHandler(json as! [String:Any])
                        }
                        catch let error as NSError {
                            print("error is:-",error)
                            completionHandler(["status":"false","message":"\(error)"])
                        }
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    completionHandler(["status":"false","message":"Error In API"])
                }
            }
            
        } else {
            SVProgressHUD.showError(withStatus: "Internet Connection not Available!")
        }
    }
    
}




 func cleanJsonToObject(data : AnyObject) -> AnyObject {
    
    let jsonObjective : Any = data
    
    if jsonObjective is NSArray {
        
        //            let array : NSMutableArray = (jsonObjective as AnyObject) as! NSMutableArray
        
        let jsonResult : NSArray = (jsonObjective as? NSArray)!
        
        
        
        let array: NSMutableArray = NSMutableArray(array: jsonResult)
        
        
        
        //            for (int i = (int)array.count-1; i >= 0; i--)
        for  i in stride(from: array.count-1, through: 0, by: -1)
        {
            let a : AnyObject = array[i] as AnyObject;
            
            if a as! NSObject == NSNull(){
                array.removeObject(at: i)
                
            } else {
                array[i] = cleanJsonToObject(data: a)
                
                //                        [self cleanJsonToObject:a];
            }
        }
        return array;
    } else
        if jsonObjective is NSDictionary  {
            
            let jsonResult : Dictionary = (jsonObjective as? Dictionary<String, AnyObject>)!
            
            
            
            let dictionary: NSMutableDictionary = NSMutableDictionary(dictionary: jsonResult)
            
            //            let dictionary : NSMutableDictionary = (jsonResult as? NSMutableDictionary<String, AnyObject>)!
            
            for  key in dictionary.allKeys {
                
                let  d : AnyObject = dictionary[key as! NSCopying]! as AnyObject
                
                if d as! NSObject == NSNull()
                {
                    dictionary[key as! NSCopying] = ""
                }
                else
                {
                    dictionary[key as! NSCopying] = cleanJsonToObject(data: d )
                }
            }
            return dictionary;
        }
        else {
            return jsonObjective as AnyObject;
    }
    
    
    //        return data
}



func anyConvertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
            
        }
    }
    return nil
}



func func_JSON_string() -> String {
    let dictionary = ["status":"false","message":"Error In API"]
    var theJSONText = ""
    if let theJSONData = try? JSONSerialization.data(withJSONObject: dictionary,options: []) {
        theJSONText = String(data: theJSONData,encoding: .ascii)!
    }
    return theJSONText
}






